from random import randint
from numpy import zeros
from sys import setrecursionlimit
from time import process_time
from os import system, path

def genAdjList(adj_matrix, size):
	nastepniki = dict()

	for i in range(len(adj_matrix)):
		new = []
		for j in range(size):
			if adj_matrix[i][j] == 1:
				new.append(j)
		nastepniki[i] = new
	return nastepniki

def genEdgeList(nastepniki):
	luki = []
	keys = list(nastepniki.keys())
	for x in keys:
		for val in nastepniki.get(x):
			luki.append((x,val))
	return luki

def genAdjMatrix(chance, size):
	chance = chance * 100
	adj_matrix = zeros([size, size], dtype=int).tolist()
	for i in range(size):
		for j in range(size):
			roll = randint(1,100)
			if roll <= chance:
				adj_matrix[i][j] = 1
	return adj_matrix

def dfsFunction(node, adj_list, pushes, pops, visited):
	global label
	pushes[node] = label
	label +=1

	visited[node] = True

	for neighbour in adj_list[node]:
		if visited[neighbour] == False:
			dfsFunction(neighbour, adj_list, pushes, pops, visited)

	pops[node] = label
	label +=1

def dfs(node, adj_list, pushes, pops, visited):
	keys = list(adj_list.keys())
	while all(visited) is not True:
		for vertex in keys:
			if visited[vertex] is False:
				dfsFunction(vertex, adj_list, pushes, pops, visited)


def countBackedgesAdjList(adj_list, pushes, pops):
	backedges=0
	global cyclic_graphs
	for vertex in adj_list:
		for neighbour in adj_list[vertex]:
			if checkForBackedge(vertex, neighbour, pushes, pops):
				backedges +=1
	if backedges != 0:
		cyclic_graphs+=1
	return backedges


def countBackedgesAdjMatrix(adj_matrix, pushes, pops):
	backedges=0
	global cyclic_graphs
	for row in range(len(adj_matrix)):
		for col in range(len(adj_matrix[row])):
			if adj_matrix[row][col] == 1:
				if checkForBackedge(row, col, pushes, pops):
					backedges+=1
	if backedges != 0:
		cyclic_graphs+=1
	return backedges


def countBackedgesEdgeList(edge_list, pushes, pops):
	backedges=0
	global cyclic_graphs
	for edge in edge_list:
		if checkForBackedge(edge[0], edge[1], pushes, pops) == True:
			backedges+=1
	if backedges != 0:
		cyclic_graphs+=1
	return backedges

def checkForBackedge(u, v, pushes, pops):
	if pushes[v] < pushes[u] < pops[u] < pops[v]:
		return True
	else:
		return False

### Globals
cyclic_graphs = 0
label = 1

def main():
	system(f"cd {path.dirname(__file__)}")
	system("mkdir results > /dev/null")
	system("cd results && rm lt2.txt lt4.txt br.txt be2listt.txt bw2matrixt.txt be2edgelstt.txt be4listt.txt be4matrixt.txt be4edgelstt.txt > /dev/null && cd .. > /dev/null")
	global label
	global cyclic_graphs
	setrecursionlimit(10000)
	sizes = [100, 200, 300, 400, 500, 600, 700, 800, 900, 1000]
        #   , 1100, 1200, 1300, 1400, 1500]

	for size in sizes:
		label = 1

		#lt2 = labeling_t_02
		#lt4 = labeling_t_04
		#be = backedges_in_adj_list_file
		#be2listt = backedges_in_adj_list_02_time_file
		#be2matrixt = backedges_in_matrix_02_time_file
		#be2edgelstt = backedges_in_edge_list_02_time_file
		#be4listt = backedges_in_adj_list_04_time_file
		#be4matrixt = backedges_in_matrix_04_time_file
		#be4edgelstt = backedges_in_edge_list_04_time_file

		### GENEROWANIE MACIERZY SĄSIEDZTWA
		adj_matrix02 = genAdjMatrix(0.2, size)
		adj_matrix04 = genAdjMatrix(0.4, size)

		### GENEROWANIE LIST NASTĘPNIKÓW
		adj_list02 = genAdjList(adj_matrix02, size)
		adj_list04 = genAdjList(adj_matrix04, size)

		## GENEROWANIE LIST ŁUKÓW
		edge_list02 = genEdgeList(adj_list02)
		edge_list04 = genEdgeList(adj_list04)

		### GENERACJA ETYKIET CZASOWYCH I MIERZENIE CZASU WYKONANIA
		visited02 = [False]*size; pushes02 = [0]*size; pops02 = [0]*size
		labeling_t_02 = process_time()
		dfs(0, adj_list02, pushes02, pops02, visited02)
		labeling_t_02 = process_time() - labeling_t_02

		visited04 = [False]*size; pushes04 = [0]*size; pops04 = [0]*size
		labeling_t_04 = process_time()
		dfs(0, adj_list04, pushes04, pops04, visited04)
		labeling_t_04 = process_time() - labeling_t_04

		### MEASURING BACKEDGE CHECKING TIME IN DIFFERENT REPRESENTATIONS
		backedges_in_adj_list_02_time = process_time()
		backedges_in_adj_list_02 = countBackedgesAdjList(adj_list02, pushes02, pops02)
		backedges_in_adj_list_02_time = process_time() - backedges_in_adj_list_02_time

		backedges_in_matrix_02_time = process_time()
		backedges_in_matrix_02 = countBackedgesAdjMatrix(adj_matrix02, pushes02, pops02)
		backedges_in_matrix_02_time = process_time() - backedges_in_matrix_02_time

		backedges_in_edge_list_02_time = process_time()
		backedges_in_edge_list_02 = countBackedgesEdgeList(edge_list02, pushes02, pops02)
		backedges_in_edge_list_02_time = process_time() - backedges_in_edge_list_02_time

		backedges_in_adj_list_04_time = process_time()
		backedges_in_adj_list_04 = countBackedgesAdjList(adj_list04, pushes04, pops04)
		backedges_in_adj_list_04_time = process_time() - backedges_in_adj_list_04_time

		backedges_in_matrix_04_time = process_time()
		backedges_in_matrix_04 = countBackedgesAdjMatrix(adj_matrix04, pushes04, pops04)
		backedges_in_matrix_04_time = process_time() - backedges_in_matrix_04_time

		backedges_in_edge_list_04_time = process_time()
		backedges_in_edge_list_04 = countBackedgesEdgeList(edge_list04, pushes04, pops04)
		backedges_in_edge_list_04_time = process_time() - backedges_in_edge_list_04_time

		### WRITING TIMES AND ACCORDING NUMBER OF VERTICES TO A FILE

		##### TASK 2 DATA #####
		lt2 = open("results/lt2.txt", "a")
		lt2.write(str(size) + " " + str(labeling_t_02) + "\n")
		lt2.close()

		lt4 = open("results/lt4.txt", "a")
		lt4.write(str(size) + " " + str(labeling_t_04) + "\n")
		lt4.close()
		##### TASK 2 DATA END #####

		###### TASK 3 DATA ######
		# column 2: 0.2
		# column 3: 0.4
		be = open("results/be.txt", "a")
		be.write(str(size) + " " + str(backedges_in_adj_list_02) + " " + str(backedges_in_adj_list_04) + "\n")
		be.close()
		###### TASK 3 DATA END ######

		####### TASK 4 DATA #######
		be2listt = open("results/be2listt.txt", "a")
		be2listt.write(str(size) + " " + str(backedges_in_adj_list_02_time) + "\n")
		be2listt.close()

		be2matrixt = open("results/be2matrixt.txt", "a")
		be2matrixt.write(str(size) + " " + str(backedges_in_matrix_02_time) + "\n")
		be2matrixt.close()

		be2edgelstt = open("results/be2edgelstt.txt", "a")
		be2edgelstt.write(str(size) + " " + str(backedges_in_edge_list_02_time) + "\n")
		be2edgelstt.close()

		be4listt = open("results/be4listt.txt", "a")
		be4listt.write(str(size) + " " + str(backedges_in_adj_list_04_time) + "\n")
		be4listt.close()

		be4matrixt = open("results/be4matrixt.txt", "a")
		be4matrixt.write(str(size) + " " + str(backedges_in_matrix_04_time) + "\n")
		be4matrixt.close()

		be4edgelstt = open("results/be4edgelstt.txt", "a")
		be4edgelstt.write(str(size) + " " + str(backedges_in_edge_list_04_time) + "\n")
		be4edgelstt.close()
		####### TASK 4 DATA END #######

	print(f"Total graphs generated: {len(sizes)*6}\nCyclic graphs: {cyclic_graphs}")

main()
