# AiSD - Algorytmy Grafowe

Implementacja algorytmów grafowych w języku Python. Do sprawozdania na Algorytmy i Struktury Danych.

## Autorzy
Michał Miłek

Sebastian Nowak

## Licencja
Projekt opublikowany jest na licencji GNU GPLv3
